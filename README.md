# RweaveExtra: Sweave drivers with extra tricks up their sleeve

The `RweaveExtraLatex` and `RtangleExtra` drivers extend the standard
`Sweave` drivers `RweaveLatex` and `Rtangle`, respectively. They are
selected through the `driver` argument of `Sweave`.

Currently, the drivers provide additional options to completely ignore
code chunks on weaving, tangling, or both. Chunks ignored on weaving
are not parsed and are written out verbatim on tangling. Chunks
ignored on tangling are processed as usual on weaving, but completely
left out of the tangled scripts.

In a literate programming workflow, the additional options allow
to include code chunks in a file such as:

- code that is not parsable by R (say, because of errors inserted for
  educational purposes);
- code in another programming language entirely (say, a shell script
  to start an analysis); 
- code for a Shiny app.

With the standard drivers, using option `eval = FALSE` results in code
being commented out in tangled scripts files. Furthermore, there is no
provision to process a chunk on weaving but leave it out on tangling.

See the file `inst/examples/example-extra.Rnw` for, well, examples.
